package mk.ukim.finki.dosie.model.exceptions;

public class JoinedSubjectNotFoundException extends Exception{

    public JoinedSubjectNotFoundException(String message) {
        super(message);
    }
}
