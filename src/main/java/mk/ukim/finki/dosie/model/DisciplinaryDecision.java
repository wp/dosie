package mk.ukim.finki.dosie.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DisciplinaryDecision {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    private DisciplinaryRecord record;

    @ManyToOne
    private DisciplinarySanction sanction;

    @Column(length = 3000)
    private String description;

}
