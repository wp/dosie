package mk.ukim.finki.dosie.model;

public enum AppRole {
    PROFESSOR, ADMIN, STUDENT;

    public String roleName() {
        return "ROLE_" + this.name();
    }
}
