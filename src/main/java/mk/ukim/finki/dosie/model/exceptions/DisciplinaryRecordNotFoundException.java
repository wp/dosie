package mk.ukim.finki.dosie.model.exceptions;

public class DisciplinaryRecordNotFoundException extends Exception{

    public DisciplinaryRecordNotFoundException(String message) {
        super(message);
    }
}
