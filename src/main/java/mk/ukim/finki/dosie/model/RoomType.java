package mk.ukim.finki.dosie.model;
public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE
}

