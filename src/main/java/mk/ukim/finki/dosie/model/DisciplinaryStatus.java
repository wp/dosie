package mk.ukim.finki.dosie.model;

public enum DisciplinaryStatus {

    REPORTED, SCHEDULED, PROCESSED
}

