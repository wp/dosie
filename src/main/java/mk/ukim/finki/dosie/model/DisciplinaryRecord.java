package mk.ukim.finki.dosie.model;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DisciplinaryRecord {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Student student;

    @ManyToOne
    private DisciplinaryType type;

    private LocalDate reportingDate;

    @Column(length = 10_000)
    private String description;

    @ManyToOne
    private Professor reporter;

    @ManyToOne
    private JoinedSubject joinedSubject;

    private Float severity;

    @Enumerated(EnumType.STRING)
    private DisciplinaryStatus status;

    private Boolean notifiedStudent;

    private LocalDateTime studentLastAccess;

    private Boolean admittedByStudent;

    @Column(length = 10_000)
    private String studentNote;

    private LocalDateTime studentLastNote;

    @ManyToOne
    private DisciplinarySanction suggestedDisciplinarySanction;

    @ManyToOne
    private DisciplinaryMeeting meeting;

    @ManyToOne
    private DisciplinaryDecision decision;

}
