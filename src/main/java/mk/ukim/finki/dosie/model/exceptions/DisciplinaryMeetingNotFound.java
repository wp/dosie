package mk.ukim.finki.dosie.model.exceptions;

public class DisciplinaryMeetingNotFound extends Exception{
    public DisciplinaryMeetingNotFound(String message) {super(message);}
}