package mk.ukim.finki.dosie.model.exceptions;

public class DisciplinaryTypeNotFoundException extends Exception{

    public DisciplinaryTypeNotFoundException(String message) {
        super(message);
    }
}
