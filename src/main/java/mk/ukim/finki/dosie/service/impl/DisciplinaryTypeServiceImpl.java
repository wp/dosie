package mk.ukim.finki.dosie.service.impl;


import mk.ukim.finki.dosie.model.DisciplinaryType;

import mk.ukim.finki.dosie.model.exceptions.CourseNotFoundException;
import mk.ukim.finki.dosie.model.exceptions.DisciplinaryTypeNotFoundException;

import mk.ukim.finki.dosie.repository.DisciplinaryTypeRepository;

import mk.ukim.finki.dosie.service.DisciplinaryTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DisciplinaryTypeServiceImpl implements DisciplinaryTypeService {
    private final DisciplinaryTypeRepository disciplinaryTypeRepository;

    public DisciplinaryTypeServiceImpl(DisciplinaryTypeRepository disciplinaryTypeRepository) {
        this.disciplinaryTypeRepository = disciplinaryTypeRepository;
    }

    @Override
    public List<DisciplinaryType> findAllCategories() {
        return this.disciplinaryTypeRepository.findAll();
    }

    @Override
    public DisciplinaryType findById(String id) throws DisciplinaryTypeNotFoundException {
        return this.disciplinaryTypeRepository.findById(id).orElseThrow(() -> new DisciplinaryTypeNotFoundException("Category is not found"));
    }
}
