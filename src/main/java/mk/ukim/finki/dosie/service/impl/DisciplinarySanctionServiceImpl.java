package mk.ukim.finki.dosie.service.impl;

import mk.ukim.finki.dosie.model.DisciplinarySanction;
import mk.ukim.finki.dosie.repository.DisciplinarySanctionRepository;
import mk.ukim.finki.dosie.service.DisciplinarySanctionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DisciplinarySanctionServiceImpl implements DisciplinarySanctionService {

    private final DisciplinarySanctionRepository disciplinarySanctionRepository;

    public DisciplinarySanctionServiceImpl(DisciplinarySanctionRepository disciplinarySanctionRepository) {
        this.disciplinarySanctionRepository = disciplinarySanctionRepository;
    }

    @Override
    public List<DisciplinarySanction> findAllSanctions() {
        return disciplinarySanctionRepository.findAll();
    }
}
