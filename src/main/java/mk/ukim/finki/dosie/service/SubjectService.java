package mk.ukim.finki.dosie.service;

import mk.ukim.finki.dosie.model.Subject;
import java.util.List;

public interface SubjectService {

    List<Subject> findAllSubjects();
}
