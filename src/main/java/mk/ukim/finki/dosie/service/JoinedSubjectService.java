package mk.ukim.finki.dosie.service;

import mk.ukim.finki.dosie.model.JoinedSubject;

import java.util.List;

public interface JoinedSubjectService {

    List<JoinedSubject> findAllJoinedSubjects();
}
