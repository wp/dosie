package mk.ukim.finki.dosie.service.impl;

import mk.ukim.finki.dosie.model.JoinedSubject;
import mk.ukim.finki.dosie.repository.JoinedSubjectRepository;
import mk.ukim.finki.dosie.service.JoinedSubjectService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JoinedSubjectServiceImpl implements JoinedSubjectService {

    private final JoinedSubjectRepository joinedSubjectRepository;

    public JoinedSubjectServiceImpl(JoinedSubjectRepository joinedSubjectRepository) {
        this.joinedSubjectRepository = joinedSubjectRepository;
    }

    @Override
    public List<JoinedSubject> findAllJoinedSubjects() {
        return joinedSubjectRepository.findAll();
    }
}
