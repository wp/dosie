package mk.ukim.finki.dosie.service;

import mk.ukim.finki.dosie.model.DisciplinarySanction;

import java.util.List;

public interface DisciplinarySanctionService {

    List<DisciplinarySanction> findAllSanctions();
}
