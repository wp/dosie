package mk.ukim.finki.dosie.service;


import mk.ukim.finki.dosie.model.DisciplinaryType;
import mk.ukim.finki.dosie.model.exceptions.DisciplinaryTypeNotFoundException;

import java.util.List;


public interface DisciplinaryTypeService {

    List<DisciplinaryType> findAllCategories();
    DisciplinaryType findById(String id) throws DisciplinaryTypeNotFoundException;
}
