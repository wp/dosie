package mk.ukim.finki.dosie.service.impl;

import mk.ukim.finki.dosie.model.Subject;
import mk.ukim.finki.dosie.repository.SubjectRepository;
import mk.ukim.finki.dosie.service.SubjectService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;

    public SubjectServiceImpl(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }

    @Override
    public List<Subject> findAllSubjects() {
        return subjectRepository.findAll();
    }
}
