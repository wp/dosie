package mk.ukim.finki.dosie.config;

import mk.ukim.finki.dosie.repository.StudentRepository;
import mk.ukim.finki.dosie.repository.UserRepository;
import mk.ukim.finki.dosie.service.ProfessorService;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Profile("cas")
@Service
public class CasUserDetailsService extends FacultyUserDetailsService implements AuthenticationUserDetailsService {


    public CasUserDetailsService(UserRepository userRepository,
                                 ProfessorService professorService,
                                 PasswordEncoder passwordEncoder,
                                 StudentRepository studentRepository) {
        super(userRepository, professorService, passwordEncoder, studentRepository);
    }

    @Override
    public UserDetails loadUserDetails(Authentication token) throws UsernameNotFoundException {
        String username = (String) token.getPrincipal();
        return super.loadUserByUsername(username);
    }
}
