package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.DisciplinaryMeeting;
import mk.ukim.finki.dosie.model.DisciplinaryMeetingParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplinaryMeetingParticipantRepository extends JpaRepository<DisciplinaryMeetingParticipant, Long> {
    List<DisciplinaryMeetingParticipant> findAllByMeeting(DisciplinaryMeeting meeting);

    void deleteAllByMeeting(DisciplinaryMeeting disciplinaryMeeting);
}
