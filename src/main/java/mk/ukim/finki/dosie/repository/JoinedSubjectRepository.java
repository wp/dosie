package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.JoinedSubject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JoinedSubjectRepository extends JpaRepository<JoinedSubject, String> {
}
