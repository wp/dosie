package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.DisciplinaryDecision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaryDecisionRepository extends JpaRepository<DisciplinaryDecision, Long> {
}
