package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.DisciplinaryType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaryTypeRepository extends JpaRepository<DisciplinaryType, String> {
}
