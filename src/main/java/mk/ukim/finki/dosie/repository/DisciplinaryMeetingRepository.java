package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.DisciplinaryMeeting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaryMeetingRepository extends JpaRepository<DisciplinaryMeeting, Long> {
}
