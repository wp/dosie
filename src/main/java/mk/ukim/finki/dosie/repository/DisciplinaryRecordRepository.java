package mk.ukim.finki.dosie.repository;



import mk.ukim.finki.dosie.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplinaryRecordRepository extends JpaRepository<DisciplinaryRecord, String> {

    List<DisciplinaryRecord> findAllByStudent(Student student);

    List<DisciplinaryRecord> findAllByMeetingIsNull();
}
