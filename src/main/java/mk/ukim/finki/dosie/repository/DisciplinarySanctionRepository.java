package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.DisciplinarySanction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinarySanctionRepository extends JpaRepository<DisciplinarySanction, Long> {
    DisciplinarySanction findByName(String name);
}
