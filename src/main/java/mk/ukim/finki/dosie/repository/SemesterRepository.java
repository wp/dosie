package mk.ukim.finki.dosie.repository;

import mk.ukim.finki.dosie.model.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SemesterRepository extends JpaRepository<Semester, String> {

}
